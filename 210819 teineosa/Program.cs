﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _210819_teineosa
{
    class Program
    {
        static void Main(string[] args)
        {
            Inimene henn = new Inimene(); //uus andmetüüp Inimene; teeme uue inimese
            henn.Nimi = "Henn Sarv"; //anname Inimesele henn ühe väärtuse
            henn.Vanus = 64;

            // saab ka:

            Inimene laura = new Inimene // siia ei pane ();
            {
                Nimi = "Laura Tikker",
                Vanus = 30,
            };

            henn.Vanus++;
            Console.WriteLine(henn);
            Console.WriteLine(laura);

            Inimene[] kõikInimesed; // massiiv, mis koosneb inimestest

            Inimene sarvik = henn; // annab muutujale sarviku muutuja henn väärtuse, mälus on ikka üks objekt aga seda defineerib kaks muutujat. Kui klassi asemel on struct siis tehakse teine objekt
            sarvik.Nimi = "Sarviktaat";
            Console.WriteLine(henn);
            Console.WriteLine(sarvik);

            //List<Inimene> inimesed = new List<Inimene> // List, mis koosneb inimestest; lõpus ei tohi olla ();
            //{ // siin teeb see nagu inimesed.Add(new Inimene {Nimi = "Malle", Vanus = 22});
            //    henn,  //lisab listi Inimese henn
            //    new Inimene {Nimi = "Ants", Vanus = 40}, //teeb uue inimese ja lisab ta listi ja lõpus ei pea koma olema
            //}; 



        }
    }

    class Inimene //kui siia panna klassi asemele struct siis kopeeritakse sisu teise, kuid mälu aadress on erinev. VALUE-tüüpi? Klassi puhul on antakse mälu aadress.
    {
        public string Nimi;
        public int Vanus;

        public override string ToString() => $"Inimene {Nimi}, kes on {Vanus} aastane"; // teeb need stringiks

    }
}
